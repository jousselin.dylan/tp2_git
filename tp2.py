class Box:

    def __init__(self):
        self._contents=[]
        self._ouvert="ouverte"
        self._capacite=None

    def add(self,truc):
        self._contents.append(truc)

    def retire(self,truc):
        self._contents.remove(truc)

    def __contains__(self,machin):
        return machin in self._contents

    def is_open(self):
        if self._ouvert=="ouverte":
            return "La boite est bien ouverte"
        return "La boite n'est pas ouverte"

    def open(self):
        self._ouvert="ouverte"

    def close(self):
        self._ouvert="fermee"

    def action_look(self):
        res=""
        if self._ouvert=="ouverte":
            for elem in self._contents:
                res+=" "+str(elem)
        else:
            return "La boite est fermée"
        return "La boite contient :"+res

    def set_capacity(self,espace):
        self._capacite=espace

    def capacity(self):
        return self._capacite
