from tp2 import *
# on veut pouvoir creer des boites
def test_box_create():
    b = Box()

def test_box_add():
    b = Box()
    b.add("truc1")
    b.add("truc2")
    assert "truc2" in b
    assert "truc3" not in b
    b.retire("truc1")
    assert "truc1" not in b

def test_box_open():
    b = Box()
    b.open()
    assert b.is_open()
    b.close()
    assert not b.close()

def test_la_boite_contient():
    b = Box()
    b.add("truc1")
    b.open()
    b.action_look()
    assert b.action_look() == "La boite contient : truc1"
    b.close()
    assert b.action_look() == "La boite est fermée"

def test_capacite():
    b = Box()
    b.set_capacity(4)
    assert b.capacity() == 4
    assert b.capacity() != 3
